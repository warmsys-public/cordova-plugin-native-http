# cordova-plugin-native-http
A workaround for WKWebView cookie bug (https://issues.apache.org/jira/browse/CB-12074) by running HTTP requests through native HTTP client.

Inspired by [Grant Pattersons post!](https://issues.apache.org/jira/browse/CB-12074?focusedCommentId=16180050&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-16180050)

## Usage
```
NativeHttp.execute(url, type, httpBody, requestOptions, successCallback, errorCallback);
```

## TODO
httpBody currently gets assigned directly to httpBody on request.
It should support setting the data as query parameters for GET requests.

Currently, assigning httpBody for a GET request will be ignored.

#### Options
``url`` string, address to make request against

``type`` string, HTTP method (GET, POST etc)

``httpBody`` string, data to add to HTTP body

``successCallback`` function, response as parameter

``errorCallback`` function, response as parameter

#### Example
```
var success = function(response) {};
var error = function(response) {};
NativeHttp.execute("http://example.com", "GET", null, {}, success, error);
```

HTTP Response:
```
{
    statusCode: 200,
    headers: {},
    data: ""
}
```

Non-HTTP error response (invalid data, url, no connection):
```
{
    statusCode: -1,
    data: "error-message"
}
```
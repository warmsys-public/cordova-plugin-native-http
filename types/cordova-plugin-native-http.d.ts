interface Window
{
    NativeHttp: {
        execute(url:string, type:string, data:any, settings:NativeHttp.Settings, successCallback:(response:NativeHttp.Response)=>void, errorCallback:(response:NativeHttp.Response)=>void);
        debug:boolean;
    };
}

declare module NativeHttp
{
    interface Settings
    {
        headers:{[header:string]:string};
    }

    interface Response
    {
        statusCode:number;
        data:string;
        headers:{[header:string]:string};
    }
}
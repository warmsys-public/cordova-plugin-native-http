var exec = require('cordova/exec');
var NativeHttp = {
    execute: function (url, type, data, options, successCallback, errorCallback) {
        var request = {
            url: url,
            type: type.toUpperCase(),
            data: data,
            options: options || {}
        };

        var onSuccess = function (result) {
            if (successCallback) {
                successCallback(result);
            }
        };

        var onError = function (result) {
            if (errorCallback) {
                errorCallback(result);
            }
        };
        exec(onSuccess, onError, 'NativeHttp', 'execute', [request]);
    }
};

var isDebugging = false;

Object.defineProperty(NativeHttp, 'debug', {
    get: function () {
        return isDebugging;
    },
    set: function (debug) {
        isDebugging = !!debug;
        exec(function(){}, function(){}, 'NativeHttp', 'debug', [isDebugging]);
    }
});

module.exports = NativeHttp;

import Foundation

@objc(CDVNativeHttp)
class CDVNativeHttp: CDVPlugin {

    private var debug:Bool = false;

    struct Arguments {
        var url: String = ""
        var httpMethod: String? = "GET"
        var data: Any?;
        var options: HttpOptions?;
    }

    struct HttpOptions {
        var headers: [String: String] = [:];
    }

    enum ArgumentError: Error {
        case badArgument(message: String)
    }

    @objc(execute:)
    func execute(_ command: CDVInvokedUrlCommand) {
        var request:URLRequest;
        var arguments: Arguments;
        do {
            try arguments = self.getArguments(command);
            try request = self.createRequestFromArguments(arguments);
        } catch ArgumentError.badArgument(let message) {
            return self.sendError(message: message, command: command)
        } catch {
            return self.sendError(message: "Something unexpected happened", command: command)
        }

        self.runRequest(request, command);
    }

    @objc(debug:)
    func debug(_ command: CDVInvokedUrlCommand) {
        if let debug = command.argument(at: 0) as? Bool {
            self.debug = debug;
            if(debug)
            {
                NSLog("'\(type(of: self))' has enabled debug logging");
            }
        }
    }

    private func createRequestFromArguments(_ arguments: CDVNativeHttp.Arguments) throws -> URLRequest {
        let url = URL(string: arguments.url);
        if (url == nil) {
            throw ArgumentError.badArgument(message: "Invalid url '\(arguments.url)'");
        }

        var request = URLRequest(url: url!);
        request.httpMethod = arguments.httpMethod;

        if let options = arguments.options {
            for (header, value) in options.headers {
                request.setValue(value, forHTTPHeaderField: header);
            }
        }

        if (request.httpMethod != "GET") {
            if let argumentData = arguments.data
            {
                if let string = argumentData as? String {
                    request.httpBody = string.data(using: .utf8);
                } else if let data = argumentData as? Data {
                    request.httpBody = data;
                } else {
                    NSLog("ERROR: Could not handle httpBody '\(argumentData)'");
                }
                if(self.debug)
                {
                    NSLog("Request data: %@", String(data: request.httpBody ?? Data(), encoding: .utf8) ?? "");
                }
            }
        }
        return request;
    }

    private func runRequest(_ request: URLRequest, _ command: CDVInvokedUrlCommand) {
        if self.debug, let url = request.url {
            NSLog("---- Request '" + url.absoluteString + "' started ----");
            NSLog("Headers: %@", request.allHTTPHeaderFields ?? [:]);
        }
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            var responseData: [String: Any] = [
                "statusCode": -1,
                "data": "",
                "headers": []
            ];

            if (error != nil) {
                if(self.debug)
                {
                    NSLog("Error: %@", error!.localizedDescription);
                }
                responseData["data"] = error!.localizedDescription;
            } else if let response = response as? HTTPURLResponse {
                if(self.debug)
                {
                    NSLog("response: %@", response);
                }
                responseData["statusCode"] = response.statusCode;
                if let data = data {
                    responseData["data"] = String(data: data, encoding: .utf8);
                }
                if let headers = response.allHeaderFields as? [String: String] {
                    // https://fetch.spec.whatwg.org/#forbidden-response-header-name
                    let unsafeHeaders = ["set-cookie", "set-cookie2"];
                    var newHeaders: [String: String] = [:];
                    for (header, value) in headers {
                        if (!unsafeHeaders.contains(where: { $0.caseInsensitiveCompare(header) == .orderedSame })) {
                            newHeaders[header] = value;
                        }
                    }
                    responseData["headers"] = newHeaders;
                }
            }

            let pluginResult = CDVPluginResult(
                    status: (200...299 ~= responseData["statusCode"] as! Int) ? CDVCommandStatus_OK : CDVCommandStatus_ERROR,
                    messageAs: responseData
            );
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
        };
        task.resume();
    }

    private func getArguments(_ command: CDVInvokedUrlCommand) throws -> Arguments {
        var arguments = Arguments();
        if (command.arguments.count == 0) {
            throw ArgumentError.badArgument(message: "Missing required parameter");
        }

        if let options = command.argument(at: 0) as? [String: Any] {
            if (options["url"] == nil) {
                throw ArgumentError.badArgument(message: "Missing option 'url'");
            }
            arguments.url = try self.getUrlFromOptions(options);
            arguments.httpMethod = self.getHttpMethodFromOptions(options);
            arguments.data = options["data"];
            arguments.options = self.getHttpOptionsFromOptions(options);
        } else {
            throw ArgumentError.badArgument(message: "Argument must be an object with structure {url,type,data,options}");
        }
        return arguments;
    }

    private func getHttpOptionsFromOptions(_ options: [String: Any]) -> CDVNativeHttp.HttpOptions? {
        var httpOptions: CDVNativeHttp.HttpOptions = HttpOptions();
        if let requestOptions = options["options"] as? [String: Any]
        {
            if let headers = requestOptions["headers"] as? [String: String] {
                httpOptions.headers = headers;
            }
        }

        return httpOptions;
    }

    private func getHttpMethodFromOptions(_ options: [String: Any]) -> String {
        if let httpMethod = options["type"] as? String {
            return httpMethod;
        }
        return "GET";
    }

    private func getUrlFromOptions(_ options: [String: Any]) throws -> String {
        if let url = options["url"] as? String {
            return url;
        }
        throw ArgumentError.badArgument(message: "url must be String");
    }

    private func sendError(message: String, command: CDVInvokedUrlCommand) {
        let responseData:[String: Any] = [
            "statusCode": -1,
            "data": message
        ];
        self.commandDelegate!.send(CDVPluginResult(
                status: CDVCommandStatus_ERROR,
                messageAs: responseData
        ), callbackId: command.callbackId)
    }
}
